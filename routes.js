const {check, validationResult} = require('express-validator');
const express = require('express');
const {matchedData} = require('express-validator');

const router = express.Router();

router.get('/', (req, res) => {
    res.render('index')
});

router.get('/contact', (req, res) => {
    res.render('contactForm', {data: {}, errors: {}})
});

router.post('/contact',
    [
        check('message')
            .isLength({min: 1})
            .withMessage('Merci de renseigner votre message')
            .trim(),
        check('email')
            .isEmail()
            .withMessage('Merci de renseigner votre email valide')
            .trim()
    ],
    (req, res, next) => {
        const errors = validationResult(req);
        console.log(req.body);
        if (!errors.isEmpty()) {
            return res.render('contactForm', {
                data: req.body,
                errors: errors.mapped()
            });
        }

        const data = matchedData(req);
        console.log('Sanitized: ', data)
    });

module.exports = router;